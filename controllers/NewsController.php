<?php
namespace app\controllers;

use app\models\Category;
use app\models\Currency;
use app\models\News;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use Yii;
use yii\rest\Controller;

/**
 * Class NewsController
 * @package app\controllers
 */
class NewsController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	protected function verbs()
	{
		return [
			'index' => ['GET', 'HEAD'],
			'category' => ['GET', 'HEAD'],

		];
	}

	/**
	 * @param null $id
	 *
	 * @return ActiveDataProvider
	 */
	public function actionIndex($id=null)
	{
		$query=News::find();
		if(isset($id))
			$query=News::find()->joinWith('categories')->
			where(['category.id'=>$this->findCategory($id)->getRecursiveIds()]);

		return new ActiveDataProvider([
			'query' => $query,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return ActiveDataProvider
	 */
	public function actionCategory($id)
	{
		return new ActiveDataProvider([
			'query' => Category::find()->where(['id'=>$this->findCategory($id)->getRecursiveIds()]),
		]);
	}

	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findCategory($id)
	{
		if (($model = Category::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}



}
