<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;

use app\models\Currency;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command update currencies table
 */
class CurrencyController extends Controller
{
    /**
     * This command update currencies table
     * @return int Exit code
     */
    public function actionIndex()
    {
	    try {
		    $xml = simplexml_load_file(Currency::UPDATE_URL);
	    } catch (Exception $e) {
		    return ExitCode::UNAVAILABLE;
	    }

	    Yii::$app->db->createCommand()->truncateTable('currency')->execute();

	    $transaction =  Yii::$app->db->beginTransaction();
	    $code=ExitCode::OK;
	    try {

		    foreach ($xml as $xmlObject)
		    {
			    $currency=new Currency();
			    $currency->setAttributesByXmlObject($xmlObject);
			    $currency->save();

		    }
		    $transaction->commit();
	    } catch(\Throwable $e) {

		    $code=ExitCode::DATAERR;
		    $transaction->rollBack();
	    }



        return $code;
    }
}
