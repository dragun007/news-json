<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_category_relation}}`.
 */
class m201123_085837_create_news_category_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news_category_relation}}', [
            'id' => $this->primaryKey(),
	        'parent_id' => $this->integer(),
            'news_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news_category_relation}}');
    }
}
