<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_category_relation".
 *
 * @property int $id
 * @property int|null $news_id
 * @property int|null $category_id
 */
class NewsCategoryRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_category_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'category_id' => 'Category ID',
        ];
    }
}
