<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $parent_id
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'parent_id' => 'Parent ID',
        ];
    }

	/**
	 * @return ActiveQuery
	 */
	public function getChild()
    {
	    return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

	/**
	 * Получаем id всех дочерних моделей и родителя
	 * @return array
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getRecursiveIds()
    {
    	$ids[]=$this->id;
    	foreach ($this->getRecursive('child') as $child)
	    {
	    	$ids[]=$child->id;
	    }
	    return $ids;
    }

	/**
	 * Рекурсивно обходит детей используя только один запрос на уровень.
	 * Вариант рекурсивной жадной загрузки.
	 * @var string $with
	 * @return array
	 * @throws \yii\base\InvalidConfigException
	 */
	private function getRecursive($with)
    {
    	$models=[];
	    $childs=$this->$with;
	    if(is_array($childs)&&count($childs)>0)
		    Yii::createObject(ActiveQuery::className(), [get_called_class()])->findWith([$with], $childs);
		    foreach ($childs as $item)
		    {
		    	$models[]=$item;
		    	$models=array_merge($models,$item->getRecursive($with));
		    }
	    return $models;
    }


}
